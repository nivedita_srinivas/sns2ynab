# SNS2YNAB Converterscript

As a passionate user of You Need A Budget (YNAB) I like to keep a neat record of all transactions in my banking account. YNAB supports importing transaction data from your bank through QIF, OFX, QFX and CSV. The first three are standards for transaction-information, however Dutch banks seem to be stubborn in supporting these formats for exporting. Most of them support CSV export, however layouts are not uniform accross institutions.

This script is intended for converting CSV-exports from [SNS-bank](https://snsbank.nl) (IBAN: SNSB) to the [YNAB CSV-structure](https://docs.youneedabudget.com/article/921-formatting-csv-file.)

## How to use
+ Export transactions from the date-range of your liking to a CSV. There is no official documentation but the procedure is described [in this forum-post (Dutch)](https://forum.snsbank.nl/mijn-sns-sns-mobiel-bankieren-app-69/hoe-maak-ik-een-transactieoverzicht-in-mijn-sns-11704.) 
+ Extract the zip file, rename the CSV file to sns.csv and place it in the same folder as the sns2ynab.py
+ Run the sns2ynab.py. Make sure your user has write permissions for the folder where you execute this script.
+ Use the created ynab.csv for importing in YNAB.

## Possible enhancements
+ Export-filename includes date
+ Export to OFX file structure
+ Add commandline arguments for in- and output files
+ Maybe build some UI

## Inspiration
+ [ING2OFX by chmistry](https://github.com/chmistry/ing2ofx)